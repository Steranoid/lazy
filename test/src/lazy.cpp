#include "jbd/lazy.hpp"

#include <gtest/gtest.h>

#include <iostream>
#include <mutex>

using namespace jbd;

TEST(lazy, testSimpleConstruct)
{
  auto value = lazy{[]() noexcept { return 1; }};
  static_assert(std::same_as<decltype(value), lazy<int, int (*)() noexcept>>);
  EXPECT_EQ(*value, 1);
}

TEST(lazy, testCaptureConstruct)
{
  auto i     = 2;
  auto value = lazy{[&i] { return i; }};
  i          = 3;
  EXPECT_EQ(*value, i);
}
