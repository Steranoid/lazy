#pragma once

#include <concepts>
#include <functional>
#include <mutex>
#include <type_traits>
#include <utility>
#include <variant>

namespace jbd {
  template <typename T, std::invocable Fn>
    requires std::same_as<T, std::invoke_result_t<Fn>> && std::negation_v<std::is_void<T>>
  class lazy {
  public:
    using type   = T;
    using getter = Fn;

    explicit lazy(Fn&& fn): m_value{std::forward<Fn>(fn)} {}

    [[nodiscard]] auto value() const -> type const&
    {
      std::call_once(m_flag, [&] { m_value = std::get<getter>(m_value)(); });
      return std::get<type>(m_value);
    }

    [[nodiscard]] auto operator*() const -> type const& { return value(); }
    [[nodiscard]] auto operator->() const -> type const* { return &value(); }

  private:
    using variant = std::variant<type, getter>;

    mutable variant        m_value = variant{};
    mutable std::once_flag m_flag  = std::once_flag{};
  };

  template <std::invocable Fn, typename T = std::invoke_result_t<Fn>>
    requires std::convertible_to<Fn, T (*)()> lazy(Fn&& fn)
  ->lazy<T, T (*)() noexcept(noexcept(fn()))>;

  template <std::invocable Fn, typename T = std::invoke_result_t<Fn>>
  lazy(Fn&& fn) -> lazy<T, Fn>;
} // namespace jbd
